//
//  ViewController.swift
//  Fortis
//
//  Created by Hector Villalba on 4/21/17.
//  Copyright © 2017 Hector Villalba. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var ofertas: [Ofertas]? = []
    var listData = [[String : AnyObject]]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchOfertas()
    }
    
    func fetchOfertas()  {
        let urlRequest = URLRequest(url: URL(string: "http://181.40.84.162:2080/Fortis.App.Api/api/ofertas/get/dec261E94ad1_8a576f0d")!)
        let task = URLSession.shared.dataTask(with: urlRequest){ (data, response, error) in
            if error != nil{
                print(error.debugDescription)
                return
            }
            self.ofertas = [Ofertas]()
            do {
                self.listData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [[String : AnyObject]]
                    
                    for ofertaJson in self.listData{
                        let oferta = Ofertas()
                        if let fotoURL = ofertaJson["FotoUrl"] as? String, let descripcion = ofertaJson["Descripcion"] as? String {
                            oferta.descripcion = descripcion
                            oferta.fotoURL = fotoURL
                        }
                        self.ofertas?.append(oferta)
                    }
            
                DispatchQueue.main.async{
                    self.tableView?.reloadData()
                }
                
            }catch let error {
                print(error)
            }
        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCellOfertas
        cell.imageViewOfertas.downloadImage(from: (self.ofertas?[indexPath.item].fotoURL)!)
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ofertas?.count ?? 0
    }

}


extension UIImageView {
    func downloadImage(from url: String){
        let urlRequest = URLRequest(url: URL(string : url)!)
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error != nil {
                print(error.debugDescription)
                return
            }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
        }
        task.resume()
    }
}

