//
//  ViewControllerEncartes.swift
//  Fortis
//
//  Created by Hector Villalba on 5/12/17.
//  Copyright © 2017 Hector Villalba. All rights reserved.
//

import UIKit

class ViewControllerEncartes: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var encartes: [Encartes]? = []
    var listaData = [[String : AnyObject]]()
    @IBOutlet weak var tableViewEncartes: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchEncartes()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnDescargar(_ sender: Any) {
        createAlert(title: "Fortis", message: "Desea descargar la imagen en HD?")
    }
    
    func createAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        //Creando el Botton 
        alert.addAction(UIAlertAction(title:  "OK", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion:nil)
            }))
        
        alert.addAction(UIAlertAction(title:  "Cancel", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion:nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func descargarImagenHD(name:String){
        //Encoding
        let image = UIImage(named: ".png")
        let imageData:NSData = UIImagePNGRepresentation(image!)! as NSData
        
        //Saved image
        UserDefaults.standard.set(imageData, forKey: name)
        
        //Decode
        //let data = UserDefaults.standard.object(forKey: name) as! NSData
        //myImageView.image = UIImage(data: data as Data)
    }
    
    func fetchEncartes(){
        let urlRequest = URLRequest(url: URL(string: "http://181.40.84.162:2080/Fortis.App.Api/api/folletos/get/dec261E94ad1_8a576f0d")!)
        let task = URLSession.shared.dataTask(with: urlRequest){ (data, response, error) in
            if error != nil{
                print(error.debugDescription)
                return
            }
            self.encartes = [Encartes]()
            do{
                self.listaData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [[String : AnyObject]]
                for encartesJson in self.listaData{
                    let encarte = Encartes()
                    if let fotoURL = encartesJson["FotoUrl"] as? String, let descripcion = encartesJson["Descripcion"] as? String{
                        encarte.descripcion = descripcion
                        encarte.fotoURL = fotoURL
                    }
                    self.encartes?.append(encarte)
                }
                DispatchQueue.main.async {
                    self.tableViewEncartes?.reloadData()
                }
            }catch let error {
                print(error)
            }
        }
        task.resume()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Encartes", for: indexPath) as! TableViewCellEncartes
        cell.imageEncartes.downloadImageEncartes(from: (self.encartes?[indexPath.item].fotoURL)!)
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.encartes?.count ?? 0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
extension UIImageView {
    func downloadImageEncartes(from url: String){
        let urlRequest = URLRequest(url: URL(string : url)!)
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error != nil {
                print(error.debugDescription)
                return
            }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
        }
        task.resume()
    }
}
