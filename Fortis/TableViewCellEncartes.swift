//
//  TableViewCellEncartes.swift
//  Fortis
//
//  Created by Hector Villalba on 5/12/17.
//  Copyright © 2017 Hector Villalba. All rights reserved.
//

import UIKit

class TableViewCellEncartes: UITableViewCell {

    @IBOutlet weak var imageEncartes: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
